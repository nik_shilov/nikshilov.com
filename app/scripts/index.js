import './../styles/main.scss'
import 'bootstrap'

if (process.env.NODE_ENV !== 'production') {
  require('./../index.pug')
}
