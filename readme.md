# Personal Portfolio Website

## Skills
* User Experience Design
* User Interface Design
* Information Architecture Design
* Interaction Design
* Workflow management
* Team leading

## Projects
* Ferr.io cryptometal exchange
* Grow Food
* Pusher
* Luxottica (Gucci, Ray-ban, Prada, D&G) eyewear
* Neosun energy

## Features:
* [Pug](https://pugjs.org) as a template engine
* [SCSS](http://sass-lang.com) preprocessor for CSS ([autoprefixer](https://github.com/postcss/autoprefixer) included)
* JS linting with [Eslint](https://eslint.org), extends [eslint-config-standard](https://github.com/standard/eslint-config-standard), includes the following plugins:
  * [import](https://github.com/benmosher/eslint-plugin-import)
  * [node](https://github.com/mysticatea/eslint-plugin-node)
  * [promise](https://github.com/xjamundx/eslint-plugin-promise)
  * [compat](https://github.com/amilajack/eslint-plugin-compat)
* CSS linting with [Stylelint](http://stylelint.io)
